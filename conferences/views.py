from django.shortcuts import render, redirect
from .forms import ConferenceForm
from django.contrib.auth.decorators import login_required
from .models import Conference


@login_required
def create_conference(request):
    if request.method == "POST":
        form = ConferenceForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ConferenceForm()
    context = {"form": form}
    return render(request, "conferences/create_conference.html", context)


@login_required
def my_conferences_list(request):
    task = Conference.objects.filter(owner=request.user)
    context = {"conferences_list": task}
    return render(request, "conferences/conferences_list.html", context)
