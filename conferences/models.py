from django.db import models
from django.conf import settings

class Conference(models.Model):
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    finish_date = models.DateTimeField()
    presentation_type = models.CharField(max_length=200)
    title = models.CharField(max_length=200)

    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="conferences",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
