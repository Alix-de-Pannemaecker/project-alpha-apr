from django.urls import path
from .views import (
    create_conference,
    my_conferences_list,
)


urlpatterns = [
    path("create/", create_conference, name="create_conference"),
    path("mine/", my_conferences_list, name="show_my_conferences"),
]
