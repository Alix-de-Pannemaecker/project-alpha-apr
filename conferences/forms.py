from django.forms import ModelForm
from .models import Conference


class ConferenceForm(ModelForm):
    class Meta:
        model = Conference
        fields = (
            "name",
            "location",
            "start_date",
            "finish_date",
            "presentation_type",
            "title",
            "owner",
        )
