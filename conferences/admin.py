from django.contrib import admin
from .models import Conference


@admin.register(Conference)
class ConferenceAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "location",
        "start_date",
        "finish_date",
        "presentation_type",
        "title",
        "owner",
        "id",
    )
