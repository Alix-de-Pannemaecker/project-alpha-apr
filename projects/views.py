from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm
import pandas as pd
from plotly.offline import plot
import plotly.express as px
import plotly.offline as opy
import plotly.graph_objs as go


# @login_required
# def projects_list(request):
#     projects = Project.objects.filter(owner=request.user)
#     context = {
#         "projects_list": projects,
#     }
#     return render(request, "projects/projects_list.html", context)


@login_required
def projects_list(request):
    projects = Project.objects.filter(owner=request.user)
    date1 = [
        x.publication_date.year
        for x in projects
    ]

    date2 = [
        1
        for year in date1
    ]

    print(projects)
    trace = go.Figure(
        data=[
            go.Bar(
                x=date1,
                y=date2,
                offsetgroup=0,
            ),
        ],

        layout=go.Layout(
            title="Number of publications per year",
            yaxis_title="Number of publications",
            xaxis_title="Year",),
    )

    bar_div = opy.plot(trace, auto_open=False, output_type="div")

    context = {"projects_list": projects, "bar_div": bar_div}

    return render(request, "projects/projects_list.html", context)


@login_required
def show_particular_project(request, id):
    details = get_object_or_404(Project, id=id)

    qs = Task.objects.all()
    projects_data = [
        {
            "Sections": x.name,
            "Start": x.start_date,
            "Finish": x.due_date,
            "Responsible": x.assignee,
        }
        for x in qs
    ]
    df = pd.DataFrame(projects_data)
    fig = px.timeline(
        df, x_start="Start", x_end="Finish", y="Sections", color="Responsible"
    )
    fig.update_yaxes(autorange="reversed")
    gantt_plot = plot(fig, output_type="div")

    context = {"particular_project": details, "plot_div": gantt_plot}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
