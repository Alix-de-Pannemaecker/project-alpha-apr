from django.db import models
from django.conf import settings


class Project(models.Model):
    name = models.CharField(max_length=200)
    authors = models.CharField(max_length=200, null=True)
    journal = models.CharField(max_length=200, null=True)
    publication_date = models.DateField(null=True)
    description = models.TextField()
    link = models.URLField(
        max_length=200,
        db_index=True,
        unique=True,
        blank=True,
        null=True,
    )

    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name
